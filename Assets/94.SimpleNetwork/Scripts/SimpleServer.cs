﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleServer : SingletonMonobehaviour<SimpleServer>
{
    private Socket serverSocket = null;

    private ArrayList Connections = new ArrayList();
    private ArrayList Buffer = new ArrayList();
    private ArrayList ByteBuffers = new ArrayList();

    public const int PortNumber = 12345;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Server Start");

        //socket 생성.
        this.serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        IPEndPoint ipLocal = new IPEndPoint(IPAddress.Any, SimpleServer.PortNumber);

        //bind
        this.serverSocket.Bind(ipLocal);

        //listen
        Debug.LogWarning("Start Listening...");
        this.serverSocket.Listen(100);
    }

    void SocketCleanUp()
    {
        if(this.serverSocket != null)
        {
            this.serverSocket.Close();
        }
        this.serverSocket = null;

        foreach(Socket client in this.Connections)
        {
            client.Close();
        }
        this.Connections.Clear();
    }

    private void OnApplicationQuit()
    {
        SocketCleanUp();
    }

    // Update is called once per frame
    void Update()
    {
        ArrayList listenList = new ArrayList();
        listenList.Add(this.serverSocket);

        Socket.Select(listenList, null, null, 1000);
        //받은 연결 요청이 있다면
        for(int i = 0; i < listenList.Count; i++)
        {
            //Accept
            Socket newConnection = ((Socket)listenList[i]).Accept();
            //클라이언트 소켓을 저장
            this.Connections.Add(newConnection);
            //
            this.ByteBuffers.Add(new ArrayList());
            Debug.Log("New Client Connected!");
        }
    }
}
