﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class SimplePacket
{
    public float mouseX = 0.0f;
    public float mouseY = 0.0f;
    
    public static byte[] ToByteArray(SimplePacket packet)
    {
        MemoryStream stream = new MemoryStream();

        BinaryFormatter formatter = new BinaryFormatter();

        //mouseX, Y를 담는다.
        formatter.Serialize(stream, packet.mouseX);
        formatter.Serialize(stream, packet.mouseY);

        return stream.ToArray();
    }

    public static SimplePacket FromByteArray(byte[] input)
    {
        MemoryStream stream = new MemoryStream();
        BinaryFormatter formatter = new BinaryFormatter();

        SimplePacket packet = new SimplePacket();
        packet.mouseX = (float)formatter.Deserialize(stream);
        packet.mouseY = (float)formatter.Deserialize(stream);

        return packet;
    }
}
