﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineSample : MonoBehaviour
{

    void Start()
    {
        Debug.Log("Start 1 : " + Time.time.ToString());
        StartCoroutine(TestCoroutine());
        Debug.Log("End 2 : " + Time.time.ToString());
    }

    IEnumerator TestCoroutine()
    {
        Debug.LogWarning("TestCoroutine 1 : " + Time.time.ToString());
        yield return new WaitForSeconds(1.0f);
        Debug.LogWarning("TestCoroutine 2 : " + Time.time.ToString());
        yield return new WaitForSeconds(2.0f);
        Debug.LogWarning("TestCoroutine 3 : " + Time.time.ToString());
        //실제 시간 기반
        yield return new WaitForSecondsRealtime(3.0f);
        //모든 한 프레임이 끝난 후
        yield return new WaitForEndOfFrame();
        //업데이트와 LateUpdate 사이에서
        yield return null;
    }
}
