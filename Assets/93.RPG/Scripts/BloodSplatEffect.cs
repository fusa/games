﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Holoville.HOTween;

public class BloodSplatEffect : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        HOTween.Init(true, true, true);
        Tweener tweener1 = HOTween.To(transform, 2, new TweenParms()
            .Prop("rotation", new Vector3(Random.Range(-360f, 360f), Random.Range(-360f, 360f), Random.Range(-360f, 360f)))
            .Prop("localScale", new Vector3(Random.Range(0.3f, 1.7f), Random.Range(0.3f, 1.7f), Random.Range(0.3f, 1.7f))).Loops(-1, LoopType.Restart));

        StartCoroutine(DestoryObject());

    }

    // Update is called once per frame
    IEnumerator DestoryObject()
    {
        yield return new WaitForSeconds(2.0f);
        Destroy(this.gameObject);
    }

}
