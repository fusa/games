﻿using UnityEngine;

public class RaycastSample : MonoBehaviour
{

    public GUISkin mySkin = null;
    private Vector3 PickPosition = Vector3.zero;

    private void FixedUpdate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit = new RaycastHit();

        //충돌이 되었다면.
        //if (Physics.Raycast(ray, out raycastHit, Mathf.Infinity) == true)
        //{
        //    PickPosition = raycastHit.point;
        //}
        //Backgorund 레이어만
        int BackgroundLayer = LayerMask.NameToLayer("Background");
        int Picklayer = 1 << BackgroundLayer;
        if(Physics.Raycast(ray, out raycastHit, Mathf.Infinity, Picklayer) == true)
        {
            PickPosition = raycastHit.point;
        }
    }

    private void OnGUI()
    {
        GUI.color = Color.black;
        GUI.skin = mySkin;
        GUILayout.Label("Point : " + PickPosition.ToString());
    }

    // Update is called once per frame
    void Update()
    {

    }
}
