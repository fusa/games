﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendMessageTestA : MonoBehaviour
{

    public GameObject Target = null;

    // Start is called before the first frame update
    void Start()
    {
        //함수 호출.
        Target.SendMessage("TestMessage", "Hello!", SendMessageOptions.DontRequireReceiver);
        gameObject.SendMessageUpwards("TestMessage", "World!", SendMessageOptions.DontRequireReceiver);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
